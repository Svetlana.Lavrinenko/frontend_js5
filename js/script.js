"use strict";

const box = document.getElementById('box');
console.log(box);

const i = document.getElementsByTagName('img');
console.log(i);

const i2 = document.getElementsByTagName('img')[2];
console.log(i2);

console.log(i[3]);


const aTag = document.getElementsByTagName('a');
console.log(aTag);


const columns = document.getElementsByClassName('colums_colum');
console.log(columns);


const images = document.querySelectorAll('img');
console.log(images);

const columnsSelector = document.querySelectorAll('.colums_colum');
console.log(columnsSelector);

columnsSelector.forEach(item => {
    console.log(item);

});


const oneImage = document.querySelector('img'); // first one
console.log(oneImage);

// conts box = document.getElementById('box'),
//     i = document.getElementsByTagName('img'),
//     aTag = document.getElementsByTagName('a'),
//     columns = document.getElementsByClassName('colums_colum'),
//     images = document.querySelectorAll('img'),
//     columnsSelector = document.querySelectorAll('.colums_colum');





console.dir(box);

box.style.backgroundColor = 'blue';



i[2].style.borderRadius = '100%';
columnsSelector[2].style.backgroundColor = 'pink';

box.style.cssText = `background-color: red; width: 500px`;

for (let j = 0; j < columnsSelector.length; j++) {
    columnsSelector[j].style.backgroundColor = 'green';
};

columnsSelector.forEach(item => {
    item.style.backgroundColor = 'pink';
});


const div = document.createElement("div");
div.classList.add('pink');

const div2 = document.createElement("div2");
div2.classList.add('black');

document.body.append(div);
//document.body.appendChild(div);

document.body.prepend(div);

//columnsSelector[2].after(div2);
columnsSelector[2].before(div2);

//document.body.insertBefore(div, i[0]);

columnsSelector[3].remove();
//document.container.removeChild(columnsSelector[1]);

images[1].replaceWith(i[0]);
//document.container.replaceChild(i[0], i[1]);


div.innerHTML = '<h1>Hello world</h1>';//text with tags

div2.textContent = 'Hello'; //only text

//div.insertAdjacentHTML('beforebegin', '<h2>hi</h2>');
//div.insertAdjacentHTML('afterbegin', '<h2>hi</h2>');
//div.insertAdjacentHTML('beforeend', '<h2>hi</h2>');
div.insertAdjacentHTML('beforebegin', '<h2>hi</h2>');